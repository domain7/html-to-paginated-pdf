require 'bundler'
Bundler.require

#defaults to using RACK_ENV
#set :environment, :production
set :run, false

set :token_salt, ((File.open("token", "rb")).read).gsub("\n","")

require './app'
run Sinatra::Application
