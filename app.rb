require 'sinatra'
require 'digest/sha1'


helpers do
  def test_hash(hash, orig_params)
    val = settings.token_salt + orig_params
    result = Digest::SHA256.hexdigest(val)
    if hash == result
      true
    else
      false
    end
  end
end

not_found do
  erb :'404'
end

get '/' do
  logger.info settings.environment
  if :development == settings.environment
    redirect '/html_to_paginated_pdf.html'
  else
    redirect "/403", 403
  end
end

get '/403' do
  erb :'403'
end

get '/html_to_paginated_pdf.html' do
  if :development == settings.environment
    erb :html_to_paginated_pdf
  else
    redirect "/403", 403
  end
end

get '/convert.pdf' do
  @uri = params[:uri]
  @hash = params[:hash]

  if @uri.nil? || !test_hash(@hash, @uri)
    redirect "/403", 403
  end

  filename = "/tmp/paginated_pdf-#{Time.now.to_i}.pdf"
  cmd = "xvfb-run -a /usr/local/bin/wkhtmltopdf --image-quality 100 -s letter -B 0 -T 0 -L 0 -R 0 --print-media-type #{@uri} #{filename}"
  #cmd = "bin/wkhtmltopdf-amd64 -s letter -B 0 -T 0 -L 0 -R 0 --print-media-type #{@uri} #{filename}"
  `#{cmd}`
  file = File.open(filename, "rb")
  contents = file.read
  File.delete(filename)
  file.close

  content_type 'application/pdf'
  contents
end
